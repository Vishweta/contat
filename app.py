from functools import wraps
from re import U
from flask import Flask, jsonify, request, abort, session, flash,redirect,url_for,make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import CORS, cross_origin
import psycopg2
import psycopg2.extras
from datetime import timedelta
from datetime import datetime
import jwt




app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:postgres@localhost/user'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['SECRET_KEY'] = 'cairocoders-ednalan'  
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=10)

CORS(app)

DB_HOST = "localhost"
DB_NAME = "user"
DB_USER = "postgres"
DB_PASS = "postgres"

conn = psycopg2.connect(dbname = DB_NAME, user = DB_USER, password = DB_PASS, host = DB_HOST,) 


db = SQLAlchemy(app)



class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    user_name = db. Column(db.String(100), nullable = False)
    user_phone = db.Column(db.Integer, nullable = True)
    user_email = db.Column(db.String(100), nullable = False)
    user_address = db.Column(db.String(100), nullable = True)
    user_password = db.Column(db.Text, nullable = False)
    # contacts = db.relationship('Contact', backref="user")

    @classmethod
    def get_by_id(cls,id):
        return cls.query.filter(cls.id==id).first()

    @classmethod
    def get_by_email(cls,email):
        return cls.query.filter(cls.user_email==email).first()
class Contact(db.Model):
    __tablename__ = "contacts"
    id = db.Column(db.Integer, primary_key=True)
    user_name = db. Column(db.String(100), nullable = False)
    user_phone = db.Column(db.Integer, nullable = True)
    user_email = db.Column(db.String(100), nullable = False)
    user_address = db.Column(db.String(100), nullable = True)
    user_country = db.Column(db.Text, nullable = False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))





    def __repr__(self):
        return "<user %r>" % self.user_name


@app.route('/')
def home():
    passhash = generate_password_hash('cairocoders')
    print(passhash)
    if 'username' in session:
        username = session['username']
        return jsonify({'message' : 'You are already logged in', 'username' : username})
    else:
        resp = jsonify({'message' : 'Unauthorized'})
        resp.status_code = 401
        return resp

@cross_origin()
@app.route('/signup', methods = ['GET','POST'])
def signup_user():
    
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    print('----------------->',request.json)
    if request.method == 'POST' and 'user_name' in request.json and 'user_password' in request.json and 'user_email' in request.json:
        
        user_data = request.json                                                           
        user_name = user_data['user_name']
        user_email = user_data['user_email']
        user_password= user_data['user_password']
        user_phone=user_data['user_phone']

        _hashed_password = generate_password_hash(user_password)

        user = Users(id=2,user_name=user_name, user_password=_hashed_password, user_email=user_email,user_phone=user_phone)

        db.session.add(user)
        db.session.commit()
        print("user")


    elif request.method == 'POST':
        
        flash('Please fill out the form!')

    
  

    return jsonify({"success": True,"response":"user signup"})

def token_required(f):
    @wraps(f)
    def decorated(*args,**kwargs):
        token =request.args.get('token')
        
        if not token:
            return jsonify({'message' : 'Token is missing!'})

        try:
            data =jwt.decode(token,app.config['SECRET_KEY'] ,algorithms=["HS256"])
            request.data = eval(str(data))
        except Exception as e:
            print('-------------------------',e) 
            return jsonify({'message' :'Token is invalid'})
        
        return f(* args, **kwargs)
    
    return decorated

@app.route('/unprotected')
def unprotected():
    return jsonify ({'message' : 'Anyone can view this!'})

@app.route('/protected')
@token_required
def protected():
    print(request.data['user'])
    return jsonify ({ 'message': 'This is only available for people with valid token'})

@app.route('/login',methods = ['GET','POST'])
def login():
    auth =request.json
    print(auth)
    if auth and auth['password'] == 12345:
        user = Users.get_by_email(auth['email'])
        if not user:
            return jsonify({'message' : 'User not found!'})
        token = jwt.encode(
        {'user_id' :user.id,  'exp' : datetime.utcnow() +timedelta(minutes =30)}, app.config['SECRET_KEY'], algorithm="HS256"
         )
        print(token)

        # token =jwt.encode({'user' : auth['email'], 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes =30)},app.config['SECRET_KEY'])
        
        # return jsonify ({'token' : token.decode('UTF-8')})

    return make_response(token,401,{'WWW-Authenticate' : 'Basic realm"Login Required"'})


@app.route('/logout',methods = ['GET','POST'])
def logout():

   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('username', None)
   return redirect(url_for('login'))


@cross_origin()
@app.route('/addusers', methods = ['GET','POST'])
def create_user():
    user_data = request.json

    user_name = user_data['user_name']
    user_phone = user_data['user_phone']
    user_email = user_data['user_email']
    user_address = user_data['user_address']
    user_password=user_password['user_password']


    user = Users(user_name=user_name, user_phone = user_phone,user_email = user_email,user_address = user_address,user_password=user_password)
    db.session.add(user)
    db.session.commit()
    

    return jsonify({"success": True,"response":"user added"})

@cross_origin()
@app.route('/create_new_contact', methods = ['POST'])
@token_required
def create_contact():
    user_id = request.data['user_id']
    return jsonify({"userid": user_id})

    contact_data = request.json
    name = contact_data['name']
    phone = contact_data['phone']
    email = contact_data['email']
    address = contact_data['address']
    country = contact_data['country']

    contact = Contact(phone =phone, email = email, address = address, country =country, user_id = user_id)
    db.session.add(contact)
    db.session.commit()
    

    return jsonify({"success": True,"response":"contact added"})




@cross_origin()    
@app.route('/contacts_list', methods = ['GET'])
def getcontacts():
     all_contacts = []
     contacts = Contact.query.all()
     for contact in contacts:
          results = {
                    "user_id":contact.id,
                    "phone":contact.phone,
                    "email":contact.email,
                    "address":contact.address,
                    "country":contact.country,
                     }
          all_contacts.append(results)

     return jsonify(
            {
                "success": True,
                "contacts": all_contacts,
                "total_contacts": len(contacts),
            }
        )

@cross_origin()  
@app.route("/users/<int:user_id>", methods = ["PATCH"])
def update_user(user_id):
    user = Users.query.get(user_id)
    user_phone = request.json['user_phone']
    user_email = request.json['user_email']

    if user is None:
        abort(404)
    else:
        user.user_phone = user_phone
        user.user_email = user_email
        db.session.add(user)
        db.session.commit()
        return jsonify({"success": True, "response": "user Details updated"})




if __name__ == '__main__':
  app.run(debug=True)
